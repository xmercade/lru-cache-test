LRU Cache Test
===================

![Language](https://img.shields.io/badge/Language-Swift-orange)


Least Recently Used Cache example
======================================

This playground creates a class named LRU that works as a least recently used cache that supports the following operations:

* get(key) - Get the value of the key if the key exists in the cache, otherwise indicate error.
* set(key, value) - Set or insert the value if the key is not already present. When the cache reached its capacity, it should invalidate the least recently used item before inserting a new item.

