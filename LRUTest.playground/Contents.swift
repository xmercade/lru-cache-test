class LRU {

    static let NO_VALUE = "NO_VALUE"
    
    let size: Int
    var keys: [String]
    var values: [String:Any]
    
    init(size: Int) {
        //  Init the cache (and the keys array) with the given size
        self.size = size
        self.keys = [String](repeating: "", count: size)
        self.values = [String:Int]()
    }
    
    func get(key: String) -> Any {
        //  Refresh the accessed status of an element by moving it to the beginning of the keys array
        if let keyIndex = self.keys.firstIndex(of: key) {
            self.keys.remove(at: keyIndex)
            self.keys.insert(key, at: 0)
        }
        
        //  If the [key:value] pair exists, return the value. Otherwise print an error and return no value.
        if let valueIndex = self.values[key] {
            return valueIndex
        } else {
            print("The key doesn't exist!")
            return LRU.NO_VALUE
        }
    }
    
    func set(key: String, value:Any) {
        if let _ = self.values[key] {
            //  Case the key exists in the cache
            self.values[key] = value
        } else {
            //  Case the key doesn't exist and we need to check the capacity
     
            //  Check if there are keys in the keys array and remove least used key from keys array and remove that key from the dictionary
            if !self.keys.isEmpty {
                let lastKey = self.keys.removeLast()
                self.values.removeValue(forKey: lastKey)
            }
            
            //  Set the new key as first element in the keys array and add the pair (key:value) to the dictionary
            self.keys.insert(key, at: 0)
            self.values[key] = value
        }
    }
    
    func clearAll() {
        //  Clear all the elements of the cache
        self.keys.removeAll()
        self.keys = [String](repeating: "", count: self.size)
        self.values.removeAll()
    }
}

//  Create a cache with 5 slots of capacity
let testLRU = LRU.init(size: 5)
print("Cache size: \(testLRU.size)")
print("---------------------------")

//  Set elements to the cache until is full, no override needed since the cache is empty
testLRU.set(key: "one", value: 1)
testLRU.set(key: "two", value: "two")
testLRU.set(key: "three", value: 3)
testLRU.set(key: "four", value: 4.0)
testLRU.set(key: "five", value: 5)

//  Print the current status of the cache, all the elements inserted are there
print("Keys: \(testLRU.keys)")
testLRU.values.keys.forEach({ print("Key: \($0) - Value: \(testLRU.values[$0] ?? LRU.NO_VALUE)")})
print("---------------------------")

//  Add a new element to the cache, the least used should be overriden
testLRU.set(key: "six", value: 6)

//  Print the current status of the cache, the first inserted value ("one") is overriden by the new element
print("Keys: \(testLRU.keys)")
testLRU.values.keys.forEach({ print("Key: \($0) - Value: \(testLRU.values[$0] ?? LRU.NO_VALUE)")})
print("---------------------------")

//  Clear the cache to start a new test
testLRU.clearAll()

//  Set elements to the cache until is full, no override needed since the cache is empty
testLRU.set(key: "one", value: 1)
testLRU.set(key: "two", value: "two")
testLRU.set(key: "three", value: 3)
testLRU.set(key: "four", value: 4.0)
testLRU.set(key: "five", value: 5)

//  Trying to get an unexisting key returns an error
var valueSix = testLRU.get(key: "six")
print("Value retrieved from the cache: \(valueSix)")
print("---------------------------")

//  Print the current status of the cache keys
print("Keys: \(testLRU.keys)")

//  Accessing existing elements of the cache to modify its accessed status
let valueOne = testLRU.get(key: "two")
let valueTwo = testLRU.get(key: "one")

//  Print the current status of the cache, the elements accessed are now moved to the beginning of the keys array
print("Keys: \(testLRU.keys)")
testLRU.values.keys.forEach({ print("Key: \($0) - Value: \(testLRU.values[$0] ?? LRU.NO_VALUE)")})
print("---------------------------")

//  Add the element "six" to the cache, now the least used element is "three" and will be overriden
testLRU.set(key: "six", value: 6)

//  Print the current status of the cache
print("Keys: \(testLRU.keys)")
testLRU.values.keys.forEach({ print("Key: \($0) - Value: \(testLRU.values[$0] ?? LRU.NO_VALUE)")})
print("---------------------------")

//  Now the element "six" can be retrieved from the cache and no error is prompted
valueSix = testLRU.get(key: "six")
print("Value retrieved from the cache: \(valueSix)")
print("---------------------------")
